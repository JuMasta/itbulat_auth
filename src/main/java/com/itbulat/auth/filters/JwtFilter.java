package com.itbulat.auth.filters;

import java.io.IOException;
import java.util.HashSet;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import com.itbulat.auth.entities.User;
import com.itbulat.auth.repositories.UserRepository;
import com.itbulat.auth.util.JwtUtil;



public class JwtFilter extends OncePerRequestFilter {

	


	Logger logger = LoggerFactory.getLogger(JwtFilter.class);
	
	
	JwtUtil jwtUtil;
	
	
	UserRepository userRepository;
	


	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
//		�������� �� ������� ��� request ���� ��� ��� ������ ����������� ������
//		String requestURI = request.getRequestURI();
		
        String authorizationHeader = request.getHeader("Authorization");

        String token = null;
        String userName = null;
        
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            token = authorizationHeader.substring(7);
            userName = jwtUtil.extractUsername(token);
        }

        if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
        	User user = userRepository.findByPhoneNumber(userName);
            UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getPhoneNumber(), user.getPassword(), new HashSet<>() );
            boolean isValidatedToken = jwtUtil.validateToken(token, userDetails);
            if (isValidatedToken) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                usernamePasswordAuthenticationToken
                        .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }
        filterChain.doFilter(request, response);
		
	}

	@Autowired
	public JwtFilter(JwtUtil jwtUtil, UserRepository userRepository) {
		super();
		this.jwtUtil = jwtUtil;
		this.userRepository = userRepository;
	}
	
	

}
