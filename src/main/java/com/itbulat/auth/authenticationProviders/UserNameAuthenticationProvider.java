package com.itbulat.auth.authenticationProviders;

import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.itbulat.auth.entities.User;
import com.itbulat.auth.repositories.UserRepository;

@Component
public class UserNameAuthenticationProvider implements AuthenticationProvider {

	Logger logger = LoggerFactory.getLogger(UserNameAuthenticationProvider.class);

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String phoneNumber = authentication.getName();
		String password = authentication.getCredentials().toString();

		User user = userRepository.findByPhoneNumber(phoneNumber);
		if (user == null)
			throw new BadCredentialsException("Invalid phoneNumber/password supplied");

		boolean passwordValidation = false;

		passwordValidation = passwordEncoder.matches(password, user.getPassword());

		if (!passwordValidation)
			throw new BadCredentialsException("Invalid phoneNumber/password supplied");

		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
				phoneNumber, password, new HashSet<>());
		return usernamePasswordAuthenticationToken;
	}
	

	@Override
	public boolean supports(Class<?> authentication) {

		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}


	
	
}
