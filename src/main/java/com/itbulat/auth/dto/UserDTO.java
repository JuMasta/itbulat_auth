package com.itbulat.auth.dto;
import javax.validation.constraints.Pattern;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class UserDTO {
	
	@NotEmpty(message = "phoneNumber can't be empty")
	@Pattern(regexp= "\\+7\\(\\d{3}\\)\\d{3}-\\d{2}-\\d{2}", message="Not correct phoneNumber")
    private String phoneNumber;
    
	@NotEmpty
    private String password;
    
	@NotEmpty
    private String name;
    
	@NotEmpty
	private String secondName;
    

	@NotNull
    private byte userType;
	
	
}
