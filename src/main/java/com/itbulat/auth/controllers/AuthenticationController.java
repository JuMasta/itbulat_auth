package com.itbulat.auth.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itbulat.auth.util.JwtUtil;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {

	Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

	@Autowired
	JwtUtil jwtUtil;

	@PostMapping("authenticate")
	public ResponseEntity<String> generateToken(Authentication authentication) {

		HttpHeaders responseHeaders = new HttpHeaders();
		String currentPrincipalName = authentication.getName();
		responseHeaders.setBearerAuth(jwtUtil.generateToken(currentPrincipalName));
		return new ResponseEntity<>(responseHeaders, HttpStatus.OK);

	}

	@PostMapping("check-request")
	public ResponseEntity<String> securePath(Authentication authentication) {
		HttpHeaders responseHeaders = new HttpHeaders();
		String currentPrincipalName = authentication.getName();
		responseHeaders.add("login",currentPrincipalName);
		return new ResponseEntity<>(responseHeaders, HttpStatus.OK);
	}

}
