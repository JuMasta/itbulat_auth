package com.itbulat.auth.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.ToString;


@Entity
@Table(name="users",
uniqueConstraints=
@UniqueConstraint(columnNames={"phoneNumber"})
		)
@Data
@ToString
public class User {
	
	
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
     private Long id;
    
	
    private String phoneNumber;
    
    private String password;
    
    private String name;
    
	private String secondName;
    
    private byte userType; 
    


	

}
