package com.itbulat.auth.configurations;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;

import com.itbulat.auth.authenticationProviders.UserNameAuthenticationProvider;
import com.itbulat.auth.filters.JwtFilter;
import com.itbulat.auth.repositories.UserRepository;
import com.itbulat.auth.util.JwtUtil;


@EnableWebSecurity
public class WebSecurityConfig {
	
	
	ProviderManager authenticationManager;
	
	@Autowired
	UserNameAuthenticationProvider userNameAuthenticationProvider;
	
	@Autowired
	JwtUtil jwtUtil;
	
	@Autowired
	UserRepository userRepository;
	
	@Bean
	public SecurityFilterChain authenticateFilterChain(HttpSecurity http) throws Exception
	{
		http.requestMatchers((request) -> { request.regexMatchers(HttpMethod.POST, "/auth/authenticate/?" ); })
		.authorizeRequests().anyRequest().authenticated()
		.and()
		.httpBasic()
		.and()
		.authenticationManager(authenticationManager)
		.csrf().disable()
		.sessionManagement()
		.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and();
		return http.build();
	}
	
	@Bean
	public SecurityFilterChain getResourcesFilterChain(HttpSecurity http) throws Exception
	{
		http.requestMatchers((request) -> { request.regexMatchers(HttpMethod.POST, "/auth/check-request/?"); })
		.authorizeRequests().anyRequest().authenticated()
		.and()
		.csrf().disable()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
		.addFilterBefore(new JwtFilter(jwtUtil, userRepository), AnonymousAuthenticationFilter.class);	
		return http.build();
	}
	
	@Autowired
	public void generateAuthenticationManager() {
		
		this.authenticationManager = new ProviderManager( getAuthenticationsProviders());
			
	}
	
	
	private List<AuthenticationProvider> getAuthenticationsProviders(){
		
		List<AuthenticationProvider> providersList = new LinkedList<>();
		providersList.add(userNameAuthenticationProvider);
		
		return providersList;
	}
	


}
