package com.itbulat.auth.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itbulat.auth.entities.User;

public interface UserRepository extends CrudRepository<User, Long> {
	
	User findByPhoneNumber(String phoneNumber);
	
	
}
